#!/bin/bash

# This script will attempt to authenticate against an Atlassian CROWD instance
# to verify that it is responsive and able to authenticate users.
#
# To use the script, you will first need to create a new application in CROWD,
# and add a single user that is permitted to authenticate against that
# application and nothing else. You'll also need to ensure that you add the IP
# address of the machine that will run the script to the list of allowed
# remote addresses in the application config in CROWD, otherwise you'll get an
# Application is not authorized error.

# =============================================================================
# Put the login credentials of the application and application user account
# here. NB: Passwords are in plain-text so make sure you're using a dedicated
# CROWD application/application user accounts that have been given restricted
# permission (ie: user can authenticate and that's it).
# =============================================================================
APPLICATION_USER="application";
APPLICATION_PASS="application password";
USER="user";
USER_PASS="user password";
URL="http://aunr-crowd-t1.ali.local:8095"

# =============================================================================
# You should not have to touch anything below this line
# =============================================================================
ACCEPT_HEADER="application/json";
BASIC_AUTH=$(echo -ne "${APPLICATION_USER}:${APPLICATION_PASS}" | base64);
CONTENT_HEADER="application/json";
DATA="{\"value\":\"${USER_PASS}\"}";

HTTP_CMD=$(curl -i -u ${APPLICATION_USER}:${APPLICATION_PASS} --write-out %{http_code} --silent --output /dev/null --data ${DATA} ${URL}/crowd/rest/usermanagement/1/authentication?username=${USER} --header "Content-Type: ${CONTENT_HEADER}" --header "Accept: ${ACCEPT_HEADER}" --header "Authorization: Basic ${BASIC_AUTH}");

if [ $HTTP_CMD -eq '200' ] || [ $HTTP_CMD -eq '401' ]
then
        echo 1;
else
        echo 0;
fi
