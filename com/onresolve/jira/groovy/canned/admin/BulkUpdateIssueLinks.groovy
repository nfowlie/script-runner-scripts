package com.onresolve.jira.groovy.canned.admin

/*
 * This File is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This File is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this File. If not, see <http://www.gnu.org/licenses/gpl-3.0.txt>.
 */

import com.atlassian.jira.bc.JiraServiceContextImpl
import com.atlassian.jira.bc.filter.SearchRequestService
import com.atlassian.jira.bc.issue.IssueService
import com.atlassian.jira.bc.issue.IssueService.IssueResult
import com.atlassian.jira.bc.issue.link.IssueLinkService
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.config.properties.APKeys
import com.atlassian.jira.config.properties.ApplicationProperties
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.link.IssueLinkType
import com.atlassian.jira.issue.link.IssueLinkTypeManager
import com.atlassian.jira.issue.search.*
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.util.ErrorCollection
import com.atlassian.jira.util.SimpleErrorCollection
import com.atlassian.jira.web.bean.PagerFilter
import com.atlassian.query.Query
import com.onresolve.jira.groovy.canned.CannedScript
import org.apache.log4j.Logger

/**
 * Bulk updates an issue link between one issue, and zero or more other issues.
 *
 * @author Nathon Fowlie &lt;nathon.fowlie&#64;gmail.com&gt;
 * @copyright Nathon Fowlie 2014. All Rights Reserved.
 *            Licensed to GLiNTECH
 * @since 02.07.2014
 */
public class BulkUpdateIssueLinks implements CannedScript {
    private static Logger LOG;
    private static boolean DEBUG;
    private static boolean INFO;
    private static boolean TRACE;

    private ApplicationUser currentUser;
    private SearchRequestService searchRequestService;
    private SearchProvider searchProvider;
    private ApplicationProperties applicationProperties;
    private IssueService issueService;
    private IssueLinkService issueLinkService;
    private IssueLinkTypeManager issueLinkTypeManager;

    private static String DEFAULT_DESCRIPTION = "Adds an issue link to multiple issues.";

    /**
     * Initializes the class logger and sets the flags used to track the logging level.
     */
    static {
        LOG = Logger.getLogger(BulkUpdateIssueLinks.class);
        DEBUG = LOG.isDebugEnabled();
        INFO = LOG.isInfoEnabled();
        TRACE = LOG.isTraceEnabled();
    }

    /**
     * Initializes a new instance of the {@link BulkUpdateIssueLinks} class.
     */
    public BulkUpdateIssueLinks() {
    }

    /**
     * Sets the value of the configured Group Picker custom field to the
     * selected group. Where a script condition has been defined, the
     * Group Picker will only be updated if the condition evaluates to
     * {@code true}.
     *
     * @param params
     * The post-function configuration information, such as the issue being
     * create/edited, the workflow action being executed and the post-function
     * parameters.
     *
     * @return
     * Returns a {@code Map} that contains the post-function configuration
     * information.
     */
    public Map<String, Object> doScript(Map<String, Object> params) {
        String searchFilter = params["SEARCH_FILTER"] as String;
        String issueKey = params["ISSUE_KEY"] as String;

        if (searchFilter == null || searchFilter.trim().isEmpty()) {
            LOG.warn("Cannot bulk update issue links, the search filter was not specified.");
            return params;
        }

        if (issueKey == null) {
            LOG.warn("Cannot bulk update issue links, the issue key was not specified.");
            return params;
        }

        return params;
    }

    /**
     * Validates the post-function parameters.
     *
     * @param params
     * A {@code Map} that contains the configured parameters.
     *
     * @param forPreview
     * Where {@code true}, indicates that the user hit the "Preview" button, or
     * that they are on the workflow transition properties page.
     *
     * @return
     * Returns an {@code ErrorCollection} that describes any validation errors
     * encountered.
     */
    public ErrorCollection doValidate(Map params, boolean forPreview) {
        if (DEBUG) {
            if (forPreview) {
                LOG.debug("Validating");
            } else {
                LOG.debug("Validating (for preview)");
            }
        }

        SimpleErrorCollection errors = new SimpleErrorCollection();

        Long filterId = params["SEARCH_FILTER"] as Long;
        Long issueLinkTypeId = params["ISSUELINKTYPE_ID"] as Long;
        boolean replaceExisting = params["REPLACE_EXISTING"] as boolean;
        String issueKey = params["ISSUE_KEY"] as String;

        LOG.debug("ISSUE_KEY: ${issueKey}");
        LOG.debug("SEARCH_FILTER: ${filterId}");
        LOG.debug("ISSUELINKTYPE_ID: ${issueLinkTypeId}");
        LOG.debug("REPLACE_EXISTING: ${replaceExisting}");

        ErrorCollection filterErrors = this.validateSearchFilter(filterId);

        if (filterErrors != null) {
            errors.addErrorCollection(filterErrors);
        }

        ErrorCollection issueLinkErrors = this.validateIssueLinkType(issueLinkTypeId);

        if (issueLinkErrors != null) {
            errors.addErrorCollection(issueLinkErrors);
        }

        IssueLinkType issueLinkType = this.getIssueLinkTypeManager().getIssueLinkType(issueLinkTypeId);

        ErrorCollection issueErrors = this.validateIssueKey(issueKey);
        if (issueErrors != null) {
            errors.addErrorCollection(issueErrors);
        }

        if (forPreview && !errors.hasAnyErrors()) {
            [output: this.getResultMessage(filterId, issueLinkType, issueKey, replaceExisting)];
        }

        return errors;
    }

    /**
     * Gets the list of categories that defines where the post-function is
     * visible in the Script Runner UI.
     *
     * Currently this script will only appear on the "Script Post-Function"
     * screen.
     *
     * @return
     * Returns a List containing the categories.
     */
    public List getCategories() {
        return [
                "ADMIN"
        ];
    }

    /**
     * Gets a brief description of the post-function. This description will be
     * shown on the "Script Post-Function" screen.
     * @return
     */
    public String getDescription() {
        return DEFAULT_DESCRIPTION;
    }

    /**
     * Gets a brief description of the post-function. This description will be
     * shown on the transition properties screen.
     *
     * @param params
     * The post-function parameters and their configuration data.
     *
     * @param forPreview
     * Where {@code true}, indicates that the post-function is being previewed.
     * This means either the user hit the "Preview" button, or they are looking
     * at the workflow transition properties page that shows all post-functions
     * configured for the given transition.
     *
     * @return
     * Returns a brief description of the post-function.
     */
    public String getDescription(Map<String, String> params, boolean forPreview) {
        if (DEBUG) {
            if (forPreview) {
                LOG.debug("Getting description");
            } else {
                LOG.debug("Getting description (for preview)");
            }
        }
        if (!forPreview) {
            return "Adds an issue link to multiple issues.";
        }

        Long filterId = params["SEARCH_FILTER"] as Long;
        Long issueLinkTypeId = params["ISSUELINKTYPE_ID"] as Long;
        boolean replaceExisting = params["REPLACE_EXISTING"] as boolean;
        String issueKey = params["ISSUE_KEY"] as String;

        LOG.debug("ISSUE_KEY: ${issueKey}");
        LOG.debug("SEARCH_FILTER: ${filterId}");
        LOG.debug("ISSUELINKTYPE_ID: ${issueLinkTypeId}");
        LOG.debug("REPLACE_EXISTING: ${replaceExisting}");

        ErrorCollection filterErrors = this.validateSearchFilter(filterId);

        if (filterErrors != null) {
            LOG.warn("Unable to bulk update the issues. ${filterErrors}");
            return DEFAULT_DESCRIPTION;
        }

        ErrorCollection issueLinkErrors = this.validateIssueLinkType(issueLinkTypeId);

        if (issueLinkErrors != null) {
            LOG.warn("Unable to bulk update the issues. ${issueLinkErrors}");
            return DEFAULT_DESCRIPTION;
        }

        IssueLinkType issueLinkType = this.getIssueLinkTypeManager().getIssueLinkType(issueLinkTypeId);

        ErrorCollection issueKeyErrors = this.validateIssueKey(issueKey);

        if (issueKeyErrors != null) {
            LOG.warn("Unable to bulk update the issues. ${issueKeyErrors}");
            return DEFAULT_DESCRIPTION;
        }

        if (forPreview) {
            return this.getResultMessage(filterId, issueLinkType, issueKey, replaceExisting);
        }

        return DEFAULT_DESCRIPTION;
    }

    /**
     * Gets the name of the post-function as it'll appear on the
     * "Script Post-function" page.
     *
     * @return
     * Returns the name of the post-function.
     */
    public String getName() {
        return "Bulk Update Issue Links";
    }

    /**
     * Gets the list of post-function parameters that require user configuration.
     *
     * @param params
     * A {@code Map} that contains the post-function parameters already
     * configured by the user.
     *
     *
     * @return
     * Returns a list that describes each post-function parameter. This list is
     * used by Script Runner to generate the post-function configuration page.
     */
    public List getParameters(Map<String, String> params) {
        return [
                [
                        Label      : "Issue Key",
                        Name       : "ISSUE_KEY",
                        Type       : "text",
                        Values     : null,
                        Description: "The issue to link the search results to."
                ],
                [
                        Label      : "Issue Link",
                        Name       : "ISSUELINKTYPE_ID",
                        Type       : "list",
                        Values     : this.getIssueLinks(),
                        Description: "The type of issue link that will be created."
                ],
                [
                        Label      : "Issue Filter",
                        Name       : "SEARCH_FILTER",
                        Type       : "list",
                        Values     : this.getOwnedFilters(),
                        Description: "The search filter that contains the list of issues that should be linked."
                ],

                [
                        Label      : "Replace existing links",
                        Name       : "REPLACE_EXISTING",
                        Type       : "bool",
                        Description: "Replace existing issue links between the source and destination issue?"
                ]
        ]
    }

    /**
     * Gets a value indicating whether the user is on the last parameter
     * configuration page.
     *
     * @param params
     * A {@code Map} that contains the list of post-function parameters.
     *
     * @return
     * Returns {@code true}.
     */
    public Boolean isFinalParamsPage(Map<String, String> params) {
        return true;
    }

    /**
     * Gets a message that can be output to the user to indicate the outcome of the operation.
     *
     * @return
     * Returns a description of the operation outcome.
     *
     * @throw NullPointerException
     * Thrown if any parameter is null or empty.
     */
    private String getResultMessage(Long filterId, IssueLinkType issueLinkType, String issueKey, boolean replaceExistingLinks) {
        if (filterId == null) {
            throw new NullPointerException("filterId cannot be null.");
        }

        if (issueLinkType == null) {
            throw new NullPointerException("issueLinkType cannot be null.");
        }

        if (issueKey == null || issueKey.trim().isEmpty()) {
            throw new NullPointerException("issueKey cannot be null or empty.");
        }

        SimpleErrorCollection errors = new SimpleErrorCollection();

        SearchRequest searchRequest = this.getSearchRequestService().getFilter(new JiraServiceContextImpl(this.getCurrentUser()), filterId);
        Query query = searchRequest.getQuery();

        SearchProvider searchProvider = this.getSearchProvider();
        SearchResults searchResults;

        try {
            searchResults = searchProvider.search(query, this.getCurrentUser(), PagerFilter.getUnlimitedFilter());
        } catch (ClauseTooComplexSearchException ctcsEx) {
            errors.addErrorMessage("Unable to bulk update the issues. ${ctcsEx.getMessage()}");
        } catch (SearchException sEx) {
            errors.addErrorMessage("Unable to bulk update the issues. ${sEx.getMessage}");
        }

        if (!errors.hasAnyErrors()) {
            String baseUrl = this.getApplicationProperties().getString(APKeys.JIRA_BASEURL);
            StringBuffer sb = new StringBuffer("<p><span style=\"color:#106E00\">Validation passed</span>. An issue link of type <strong>${issueLinkType.getOutward()}</strong> will be added from <strong>${issueKey}</strong> to the following issues");

            if (replaceExistingLinks) {
                sb.append(" and any existing issue links between the source &amp; destination issues will be replaced:</p>");
            } else {
                sb.append(":</p>");
            }

            sb.append("<p>&nbsp;</p>");

            for (Issue issue : searchResults.getIssues()) {
                sb.append("<p><a href=\"");
                sb.append(baseUrl);
                sb.append("/browse/");
                sb.append(issue.getKey());
                sb.append("\" target=\"_blank\">");
                sb.append(issue.getKey());
                sb.append("</a>: ");
                sb.append(issue.getSummary());
                sb.append("</p>");
            };

            return sb.toString();
        }

        return "One or more errors occurred during the operation. Please see the application logs for detailed information.";
    }

    /**
     * Gets an {@code ApplicationProperties} instance that can be used to
     * retrieve JIRAs base url.
     *
     * @return
     * Returns an {@code ApplicationProperties} instance.
     */
    private ApplicationProperties getApplicationProperties() {
        if (this.applicationProperties == null) {
            this.applicationProperties = ComponentAccessor.getApplicationProperties();
        }

        return this.applicationProperties;
    }

    /**
     * Gets the currently logged in user.
     *
     * @return
     * Returns an {@code ApplicationUser} instance.
     */
    private ApplicationUser getCurrentUser() {
        if (this.currentUser == null) {
            JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext();
            this.currentUser = authenticationContext.getUser();
        }

        return this.currentUser;
    }

    /**
     * Gets the list of issue links that are visible to the current user.
     *
     * @return
     * Returns a HashMap of available issue links, where the key is the issue
     * id, and the value is the name of the issue link.
     */
    private Map<Long, String> getIssueLinks() {
        Collection<IssueLinkType> issueLinkTypes = this.getIssueLinkService().getIssueLinkTypes();

        if (issueLinkTypes == null || !issueLinkTypes.size() == 0) {
            return [:];
        }


        Map<Long, String> issueLinkList = new HashMap<Long,String>();
        issueLinkList.put(0, "");

        for (IssueLinkType issueLinkType : issueLinkTypes) {
            issueLinkList.put(issueLinkType.getId(), issueLinkType.getOutward());
        }

        return issueLinkList;
    }

    /**
     * Gets a {@code IssueLinkTypeManager} that can be used to determine the
     * type of issue link used by an issue.
     *
     * @return
     * Returns a {@code IssueLinkTypeManager} instance.
     */
    private IssueLinkTypeManager getIssueLinkTypeManager() {
        if (this.issueLinkTypeManager == null) {
            this.issueLinkTypeManager = ComponentAccessor.getComponent(IssueLinkTypeManager.class);
        }

        return this.issueLinkTypeManager;
    }


    /**
     * Gets an {@code IssueLinkService} that can be used to manage issue links.
     *
     * @return
     * Returns an {@code IssueLinkService} instance.
     */
    private IssueLinkService getIssueLinkService() {
        if (this.issueLinkService == null) {
            this.issueLinkService = ComponentAccessor.getComponent(IssueLinkService.class);
        }

        return this.issueLinkService;
    }

    /**
     * Gets an {@code IssueService} instance that can be used to manage JIRA
     * issues that the current user is permitted to modify.
     *
     * @return
     * Returns a {@code IssueService} instance.
     */
    private IssueService getIssueService() {
        if (this.issueService == null) {
            this.issueService = ComponentAccessor.getIssueService();
        }

        return this.issueService;
    }

    /**
     * Gets the list of filters available to the current user.
     *
     * @return
     * Returns a {@code Map} where the key and value is the name of the search
     * filter.
     */
    private Map<Long, String> getOwnedFilters() {
        Map<Long, String> filterList = new HashMap<Long, String>();
        filterList.put(0L, "");

        SearchRequestService searchRequestService = this.getSearchRequestService();

        searchRequestService.getOwnedFilters(this.getCurrentUser()).each { f ->
            filterList.put(f.getId(), f.getName());
        };

        return new TreeMap<Long, String>(filterList);
    }

    /**
     * Gets a {@code SearchProvider} instance that can be used to execute JQL
     * searches.
     *
     * @returns
     * Returns a {@code SearchProvider} instance.
     */
    private SearchProvider getSearchProvider() {
        if (this.searchProvider == null) {
            this.searchProvider = ComponentAccessor.getComponent(SearchProvider.class);
        }

        return this.searchProvider;
    }

    /**
     * Gets a {@code SearchRequestService} instance that can be used to manage
     * and retrieve search filters.
     *
     * @return
     * Returns a {@code SearchRequestService} instance.
     */
    private SearchRequestService getSearchRequestService() {
        if (this.searchRequestService == null) {
            this.searchRequestService = ComponentAccessor.getComponent(SearchRequestService.class);
        }

        return this.searchRequestService;
    }
    /**
     * Validates the user provided issue key.
     *
     * @return
     * Returns an {@code ErrorCollection} containing any validation errors that
     * are encountered, or {@code null} if validation passes.
     */
    private ErrorCollection validateIssueKey(String issueKey) {
        SimpleErrorCollection errors = new SimpleErrorCollection();

        if (issueKey == null || issueKey.trim().isEmpty()) {
            errors.addError("ISSUE_KEY", "Please specify the source issue key.");
        } else {
            IssueResult issueResult = this.getIssueService().getIssue(this.getCurrentUser().getDirectoryUser(), issueKey);

            if (!issueResult.isValid()) {
                errors.addError("ISSUE_KEY", "Issue key not found.");
            }
        }

        return errors.hasAnyErrors() ? errors : null;
    }

    /**
     * Validates the user provided issue link to ensure its available.
     *
     * @return
     * Returns an {@code ErrorCollection} containing any validation errors that
     * are encountered, or {@code null} if validation passed.
     */
    private ErrorCollection validateIssueLinkType(Long issueLinkTypeId) {
        SimpleErrorCollection errors = new SimpleErrorCollection();

        if (issueLinkTypeId == null || issueLinkTypeId == 0) {
            errors.addError("ISSUELINKTYPE_ID", "Please provide the issue link type.");
        } else {
            IssueLinkType issueLinkType = this.getIssueLinkTypeManager().getIssueLinkType(issueLinkTypeId);

            if (issueLinkType == null) {
                errors.addError("ISSUELINKTYPE_ID", "Issue link not found.");
            }
        }

        return errors.hasAnyErrors() ? errors : null;
    }

    /**
     * Validates the user provided search filter to ensure its available for use.
     *
     * @return
     * Returns an {@code ErrorCollection} containing any validation errors that
     * are encountered, or {@code null} if validation passes.
     */
    private ErrorCollection validateSearchFilter(Long filterId) {
        SimpleErrorCollection errors = new SimpleErrorCollection();

        if (filterId == null || filterId == 0) {
            errors.addError("SEARCH_FILTER", "Please specify the search filter.");
        } else {
            SearchRequest searchRequest = this.getSearchRequestService().getFilter(new JiraServiceContextImpl(this.getCurrentUser()), filterId);

            if (searchRequest == null) {
                errors.addError("SEARCH_FILTER", "Search filter not found.");
            }
        }

        return (errors.hasAnyErrors()) ? errors : null;
    }
}