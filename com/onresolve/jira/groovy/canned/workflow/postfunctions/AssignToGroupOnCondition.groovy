package com.onresolve.jira.groovy.canned.workflow.postfunctions

import com.atlassian.crowd.embedded.api.Group
import com.atlassian.jira.bc.issue.IssueService
import com.atlassian.jira.bc.issue.IssueService.IssueResult
import com.atlassian.jira.bc.issue.IssueService.UpdateValidationResult
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.IssueInputParameters
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.security.JiraAuthenticationContext
import com.atlassian.jira.security.groups.DefaultGroupManager
import com.atlassian.jira.security.groups.GroupManager
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.util.ErrorCollection
import com.atlassian.jira.util.SimpleErrorCollection
import com.onresolve.jira.groovy.canned.CannedScript
import com.onresolve.jira.groovy.canned.utils.ConditionUtils
import org.apache.log4j.Logger

/**
 * Sets the value of a Group Picker custom field with an optional script runner
 * condition to determine whether the field should be set.
 *
 * To use, add a "Script Post-Function" to a workflow transition and select the
 * "Assign to Group based on condition" post function. On the configuration page
 * select the Group Picker custom field that will have its value set, and the name
 * of the group that will be assigned.
 *
 * @author Nathon Fowlie &lt;nathon.fowlie&#64;gmail.com&gt;
 * @since 23.06.2014
 */
public class AssignToGroupOnCondition implements CannedScript {
    private static Logger LOG;
    private static boolean DEBUG;
    private static boolean INFO;
    private static boolean TRACE;

    private ApplicationUser currentUser;
    private CustomFieldManager customFieldManager;
    private GroupManager groupManager;

    /**
     * Initializes the class logger and sets the flags used to track the logging level.
     */
    static {
        LOG = Logger.getLogger(AssignToGroupOnCondition.class);
        DEBUG = LOG.isDebugEnabled();
        INFO = LOG.isInfoEnabled();
        TRACE = LOG.isTraceEnabled();
    }

    /**
     * Initializes a new instance of the {@link AssignToGroupOnCondition} class.
     */
    public AssignToGroupOnCondition() {
    }

    /**
     * Sets the value of the configured Group Picker custom field to the
     * selected group. Where a script condition has been defined, the
     * Group Picker will only be updated if the condition evaluates to
     * {@code true}.
     *
     * @param params
     * The post-function configuration information, such as the issue being
     * create/edited, the workflow action being executed and the post-function
     * parameters.
     *
     * @return
     * Returns a {@code Map} that contains the post-function configuration
     * information.
     */
    public Map<String, Object> doScript(Map<String, Object> params) {
        MutableIssue issue = params['issue'] as MutableIssue;

        if (issue == null) {
            LOG.warn("Unable to auto-assign issue to a group. No issue object was provided.");
            return params;
        }

        if (!ConditionUtils.processCondition(params[ConditionUtils.FIELD_CONDITION] as String, issue, false, params)) {
            if (DEBUG) {
                LOG.debug("Script condition returned false, group will not be assigned.");
            }

            return params;
        }

        String fieldId = params['GROUP_FIELD'] as String;
        String groupName = params['SELECTED_GROUP'] as String;

        if (fieldId == null || fieldId.trim().isEmpty()) {
            LOG.warn("Unable to auto-assign ${issue.getKey()} to a group. The Group Picker field to update was not provided.");
            return params;
        }

        if (groupName == null) {
            LOG.warn("Unable to auto-assign ${issue.getKey()} to a group. The Group name was not specified.");
            return params;
        }

        Group assignedGroup = this.getGroupManager().getGroupObject(groupName);

        if (assignedGroup == null) {
            LOG.warn("Unable to auto-assign ${issue.getKey()} to a group. The nominated roup doesn't appear to exist.");
            return params;
        }

        CustomField customField = this.getCustomFieldManager().getCustomFieldObject(fieldId);

        if (customField == null) {
            LOG.warn("Unable to auto-assign ${issue.getKey()} to a group. The Group Picker field could not be found.");
            return params;
        }

        issue.setCustomFieldValue(customField, [assignedGroup]);

        IssueService issueService = ComponentAccessor.getIssueService();

        IssueInputParameters inputParams = issueService.newIssueInputParameters();
        inputParams.addCustomFieldValue(customField.getId(), groupName);

        UpdateValidationResult result = issueService.validateUpdate(
                this.getCurrentUser().getDirectoryUser(),
                issue.getId(),
                inputParams
        );

        if (!result.isValid()) {
            LOG.warn("Unable to auto-assign ${issue.getKey()} to ${groupName}. ${result.getErrorCollection().getErrors()}. ${result.getErrorCollection().getErrorMessages()}");
            return params;
        }

        IssueResult issueResult = issueService.update(currentUser.getDirectoryUser(), result, EventDispatchOption.DO_NOT_DISPATCH, false);

        if (!issueResult.isValid()) {
            LOG.warn("Unable to auto-assign ${issue.getKey()} to ${groupName}. ${issueResult.getErrorCollection().getErrors()}. ${result.getErrorCollection().getErrorMessages()}");
            return params;
        }

        return params;
    }

    /**
     * Validates the post-function parameters.
     *
     * @param params
     * A {@code Map} that contains the configured parameters.
     *
     * @param forPreview
     * Where {@code true}, indicates that the user hit the "Preview" button, or
     * that they are on the workflow transition properties page.
     *
     * @return
     * Returns an {@code ErrorCollection} that describes any validation errors
     * encountered.
     */
    public ErrorCollection doValidate(Map<String, String> params, boolean forPreview) {
        if (DEBUG) {
            LOG.debug("Validating AssignToGroupOnCondition post-function parameters. The configured parameters are - ${params}");
        }

        ErrorCollection errors = new SimpleErrorCollection();

        String fieldId = params['GROUP_FIELD'] as String;
        String groupName = params['SELECTED_GROUP'] as String;

        if (fieldId == null || fieldId.trim().isEmpty()) {
            errors.addError("GROUP_FIELD", "The Group Picker field must be specified.");
        }

        if (groupName == null) {
            errors.addError("SELECTED_GROUP", "The group to be assigned must be specified.");
        }

        return errors;
    }

    /**
     * Gets the list of categories that defines where the post-function is
     * visible in the Script Runner UI.
     *
     * Currently this script will only appear on the "Script Post-Function"
     * screen.
     *
     * @return
     * Returns a List containing the categories.
     */
    public List getCategories() {
        return [
                "Function"
        ];
    }

    /**
     * Gets a brief description of the post-function. This description will be
     * shown on the "Script Post-Function" screen.
     * @return
     */
    public String getDescription() {
        return "Assigns the issue to a Group when the provided condition evaluates to true.";
    }

    /**
     * Gets a brief description of the post-function. This description will be
     * shown on the transition properties screen.
     *
     * @param params
     * The post-function parameters and their configuration data.
     *
     * @param forPreview
     * Where {@code true}, indicates that the post-function is being previewed.
     * This means either the user hit the "Preview" button, or they are looking
     * at the workflow transition properties page that shows all post-functions
     * configured for the given transition.
     *
     * @return
     * Returns a brief description of the post-function.
     */
    public String getDescription(Map<String, String> params, boolean forPreview) {
        String fieldId = params["GROUP_FIELD"] as String;
        CustomField customField = this.getCustomFieldManager().getCustomFieldObject(fieldId);

        String groupName = params["SELECTED_GROUP"] as String;
        Group assignedGroup = this.getGroupManager().getGroupObject(groupName);

        String fieldCondition = params["FIELD_CONDITION"] as String;

        if (fieldCondition == null || fieldCondition.trim().isEmpty()) {
            return "<strong>${customField.getName()}</strong> will be set to <strong>${assignedGroup.getName()}</strong>.";
        } else {
            return "<strong>${customField.getName()}</strong> will be set to <strong>${assignedGroup.getName()}</strong> when the following expression evaluates to true: <strong>${fieldCondition}</strong>";
        }
    }

    /**
     * Gets the name of the post-function as it'll appear on the
     * "Script Post-function" page.
     *
     * @return
     * Returns the name of the post-function.
     */
    public String getName() {
        return "Assign to Group based on condition";
    }

    /**
     * Gets the list of post-function parameters that require user configuration.
     *
     * @param params
     * A {@code Map} that contains the post-function parameters already
     * configured by the user.
     *
     * @return
     * Returns a list that describes each post-function parameter. This list is
     * used by Script Runner to generate the post-function configuration page.
     */
    public List getParameters(Map<String, String> params) {
        return [
                [
                        Label      : "Group Field",
                        Name       : "GROUP_FIELD",
                        Type       : "list",
                        Values     : this.getAllGroupPickerFields(),
                        Description: "The group picker field to be set."
                ],
                [
                        Label      : "Group",
                        Name       : "SELECTED_GROUP",
                        Type       : "list",
                        Values     : this.getAllGroups(),
                        Description: "The group that the issue will be assigned to."
                ],
                ConditionUtils.getConditionParameter()
        ]
    }

    /**
     * Gets a value indicating whether the user is on the last parameter
     * configuration page.
     *
     * @param params
     * A {@code Map} that contains the list of post-function parameters.
     *
     * @return
     * Returns {@code true}.
     */
    public Boolean isFinalParamsPage(Map<String, String> params) {
        return true;
    }

    /**
     * Gets all the available groups defined in JIRA. This is used to populate
     * the "Group" parameter on the post-function configuration page.
     *
     * @return
     * Returns a {@code Map} where the key and value is the name of each JIRA
     * Group.
     */
    private Map<String, String> getAllGroups() {
        Map<String, String> groups = new HashMap<String, String>();

        this.getGroupManager().getAllGroups().each { group ->
            groups.put(group.getName(), group.getName());
        }

        return groups;
    }

    /**
     * Gets all available Group Picker custom fields. This includes both single
     * and multi select Group Pickers.
     *
     * @return
     * Returns a @{code Map} where the key and value is the name of the Group
     * Picker custom field.
     */
    private Map<String, String> getAllGroupPickerFields() {
        Map<String, String> fields = new HashMap<String, String>();

        this.getCustomFieldManager().getCustomFieldObjects().each { cf ->
            if (cf.getCustomFieldType().getName().contains("Group Picker")) {
                fields.put(cf.getId().toString(), cf.getName());
            }
        };

        return fields;
    }

    /**
     * Gets the currently logged in user.
     *
     * @return
     * Returns an {@code ApplicationUser} instance.
     */
    private ApplicationUser getCurrentUser() {
        if (this.currentUser == null) {
            JiraAuthenticationContext authenticationContext = ComponentAccessor.getJiraAuthenticationContext();
            this.currentUser = authenticationContext.getUser();
        }

        return this.currentUser;
    }

    /**
     * Gets a {@code CustomFieldManager} instance that's used to update the
     * Group Picker custom field.
     *
     * @return
     * Returns a {@code CustomFieldManager} instance.
     */
    private CustomFieldManager getCustomFieldManager() {
        if (this.customFieldManager == null) {
            this.customFieldManager = ComponentAccessor.getCustomFieldManager();
        }

        return this.customFieldManager;
    }

    /**
     * Gets a {@code GroupManager} instance that is used to retrieve the list
     * of groups that can be assigned to the selected Group Picker custom
     * field.
     *
     * @return
     * Returns a {@code GroupManager} instance.
     */
    private GroupManager getGroupManager() {
        if (this.groupManager == null) {
            this.groupManager = new DefaultGroupManager(ComponentAccessor.getCrowdService());
        }

        return this.groupManager;
    }
}
