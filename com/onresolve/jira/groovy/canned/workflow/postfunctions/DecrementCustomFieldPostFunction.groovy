package com.onresolve.jira.groovy.canned.workflow.postfunctions.nathonfowlie;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.SimpleErrorCollection;

import au.id.nathonfowlie.jira.scriptrunner.postfunctions.ParameterDescriptor;
import au.id.nathonfowlie.jira.utils.CustomFieldMathOperation;

import com.onresolve.jira.groovy.canned.CannedScript;
import com.onresolve.jira.groovy.canned.utils.ConditionUtils;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.ParsePosition;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 * Script Runner Post-Function that decrements the value of a custom field. 
 *
 * Where the field does not contain a value, then the default value as defined
 * in the post-function configuration provided by the user will be used as the
 * initial 'seed' value.
 *
 * If an error occurs, depending on the data type of the custom field  it will 
 * be set to one of the following:
 *
 * Data Type | Value set on Error
 * ----------|-------------------
 * String    | 'NaN'
 * Integer   | Integer.MIN_VALUE (-2,147,483,647)
 * Double    | Double.MIN_VALUE (0x0.0000000000001p-1022)
 *
 * @author Nathon Fowlie &lt;nathon.fowlie&#64;gmail.com&gt;
 * @since 21/03/2014
 * @version 1.0
 *
 * Change Log:
 *    21/03/2014 nfowlie: Refactored code, moved into seperate 
 *                        CustomFieldMathOperation class to 
 *                        consolidate math orientated custom field
 *                        post-functions.
 */
public class DecrementCustomFieldPostFunction implements CannedScript {
    private static final Logger log = Logger.getLogger(DecrementCustomFieldPostFunction.class);

    // This is the post-function descriptor for the parameter that determines
    // what the default "seed" value should be if the custom field does not
    // have a value.
    private static final ParameterDescriptor defaultValueParameter = new ParameterDescriptor(
        "Default Value", 
        "Default Value", 
        "shorttext", 
        "The default value to be used where the field is blank."
    );

    // This is the post-function descriptor for the parameter that determines
    // which custom field will be decremented.
    private static final ParameterDescriptor customFieldParameter = new ParameterDescriptor(
      "Custom Field", 
      "Custom Field", 
      "list", 
      "The custom field to be decremented."
    );

    // This is the post-function descriptor for the parameter that determines 
    // how much to decrement the field by.
    private static final ParameterDescriptor decrementParameter = new ParameterDescriptor(
        "Decrement Value", 
        "Decrement Value", 
        "shorttext", 
        "The amount to decrement the field by."
    );

    // This determines where in the JIRA/Script-Runner UI the post-function 
    // will appear. Refer to the Script Runner documentation for a list of
    // available values.
    private static final List CATEGORIES = Arrays.asList("Function");

    private final CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();

    /**
     * Validates the post-function configuration provided by the user.
     *
     * @param params
     *          The post-function parameters provided by the user.
     * @param forPreview
     *          Indicates whether the user is attempting to preview 
     *          (verify) the configuration.
     *
     * @return
     *          Returns an ErrorCollection that contains any validation
     *          errors that were detected.
     */
    public ErrorCollection doValidate(Map params, boolean forPreview) {
        if (log.isDebugEnabled()) {
          String message = String.format(
              LogMessages.VALIDATING_PARAMETERS,              
              Arrays.toString(this.getParameters().toArray())
            );

            log.debug(message);
        }

        ErrorCollection errors = new SimpleErrorCollection();
        ErrorCollection parameterValidationErrors = new SimpleErrorCollection();
        
        if (params[defaultValueParameter.getName()] && !params[defaultValueParameter.getName()].trim().isEmpty()) {
            // The "Default Value" parameter value provided by the user is passed 
            // as a String so we need to verify that it's a valid number.
            parameterValidationErrors = this.validateNumericParameter(
                params[defaultValueParameter.getName()],
                defaultValueParameter.getLabel()
            );

            if (parameterValidationErrors && parameterValidationErrors.hasAnyErrors()) {
                errors.addErrors(parameterValidationErrors.getErrors());
            }
        }

        if (!params[customFieldParameter.getName()] || params[customFieldParameter.getName()].trim().isEmpty()) {
            errors.addError(customFieldParameter.getLabel(), UIMessages.CUSTOMFIELD_IS_REQUIRED);
        }

        if (!params[decrementParameter.getName()]) {
            errors.addError(decrementParameter.getLabel(), UIMessages.DECREMENT_IS_REQUIRED);
        }

        // The "Decrement" parameter value provided by the user is passed 
        // as a String so we need to verify that it's a valid number.
        parameterValidationErrors = this.validateNumericParameter(params[decrementParameter.getName()], decrementParameter.getLabel());

        if (parameterValidationErrors && parameterValidationErrors.hasAnyErrors()) {
            errors.addErrors(parameterValidationErrors.getErrors());
        }

        if (!this.isNumeric(params[decrementParameter.getName()])) {
          errors.addError(decrementParameter.getLabel(), UIMessages.DECREMENT_NOT_NUMERIC);
        }

        return errors;
     }

    /**
     * Gets the list of areas where the script can be used.
     *
     * @return
     *      A list of categories describing where the script can be used.
     */
    public List getCategories() {
        if (log.isDebugEnabled()){
            String message = String.format(
              LogMessages.CATEGORIES, 
              this.getName(), 
              Arrays.toString(CATEGORIES.toArray())
            );

            log.debug(message);
        }

        return CATEGORIES;
    }

    /**
     * Gets a generic description of the post-function.
     *
     * @return
     *         A generic description.
     */
    public String getDescription() {
        return UIMessages.DESCRIPTION;
    }

    /**
     * Gets a description of the post-function.
     *
     * @param params
     *           The parameter values provided to the post-function.
     * @param forPreview
     *           Indicates whether the user is previewing the function.
     *
     * @return
     *           A description of the post-function.
     */
    public String getDescription(Map params, boolean forPreview) {
        if (params[customFieldParameter.getName()]) {
            CustomField customField = this.customFieldManager.getCustomFieldObject(params[customFieldParameter.getName()]);

            return String.format(
                UIMessages.DETAILED_DESCRIPTION,
                customField.getName(), 
                params[decrementParameter.getName()], 
                params[defaultValueParameter.getName()], 
                params[decrementParameter.getName()]
            );
        }

        return UIMessages.DESCRIPTION;
    }

    /**
     * Gets the Url where users may obtain help and support.
     *
     * @return
     *         A string containing the help and support Url.
     */
    public String getHelpUrl() {
      return UIMessages.HELP_URL;
    }

    /**
     * Gets the name of the post-function.
     *
     * @return
     *        The post-function name.
     */
    public String getName() {
        return UIMessages.NAME;
    }

    /** 
     * Gets the list of post-function parameter descriptors that describe what 
     * parameters are required by the post-function.
     *
     * @param params
     *          The list of parameters required by the post-function.
     *
     * @return List
     *          returns a list of parameter descriptors.
     */
    public List getParameters(Map params) {
        return [
            [
                Label: defaultValueParameter.getLabel(),
                Name: defaultValueParameter.getName(),
                Type: defaultValueParameter.getFieldType(),
                Description: defaultValueParameter.getDescription()
            ],
            [
                Label: customFieldParameter.getLabel(),
                Name: customFieldParameter.getName(),
                Type: customFieldParameter.getFieldType(),
                Description: customFieldParameter.getDescription(),
                Values: this.getCustomFields()
            ],
            [
                Label: decrementParameter.getLabel(),
                Name: decrementParameter.getName(),
                Type: decrementParameter.getFieldType(),
                Description: decrementParameter.getDescription()
            ],
            ConditionUtils.getConditionParameter()
        ];
     }

    /**
      * This is the main entry point into the workflow post function. It is 
      * responsible for decrementing the value of the provided field.
      *
      * @param params
      *         The post function parameters.
      *
      * @return
      *         This function will always return an empty Map object.
      */
     public Map doScript(Map params) {
          MutableIssue issue = params['issue'] as MutableIssue;

          if (!issue) {
              this.log.warn(LogMessages.ISSUE_NOT_FOUND);
              return [:];
          }

          CustomField customField = this.customFieldManager.getCustomFieldObject(params[customFieldParameter.getName()]);

          if (!ConditionUtils.processCondition(params[ConditionUtils.FIELD_CONDITION] as String, issue, false, params)) {
              if (log.isDebugEnabled()) {
                  String message = String.format(
                      LogMessages.POSTFUNCTION_CONDITION_FAILED,
                      customField.getName()
                  );

                  log.debug(message);
              }

              return [:];
          }      

          String currentValue = issue.getCustomFieldValue(customField);

          CustomFieldMathOperation mathOperation = new CustomFieldMathOperation(issue);

          mathOperation.decrementCustomField(
              customField, 
              params[decrementParameter.getName()], 
              params[defaultValueParameter.getName()]
          );

          return [:];
     }

    






     

     /**
      * Indicates if there are additional parameters to be configured.
      *
      * @param params
      *         The list of configured parameters.
      *
      * @return
      *         Always returns true.
      */
    public Boolean isFinalParamsPage(Map params) {
        return true;
    }

    /**
     * Gets the list of all custom fields.
     *
     * @return
     *      Returns a HashMap, where the key is the custom field id, and the 
     *      value is the name of the custom field.
     */
    private Map<String, String> getCustomFields() {
      List<CustomField> customFields = this.customFieldManager.getCustomFieldObjects();

      Map<String, String> availableFields = [:] as Map<String, String>;
      availableFields.put("",""); // So the default selected dropdown item is empty.

      customFields.findAll { CustomField field ->
        !field.getName().startsWith("?")
      }.sort { CustomField a, CustomField b ->
        if (a.getName() && b.getName()) {
          return a.getName().compareToIgnoreCase(b.getName());
        } else {
          return -1;
        }
      }.each {
        availableFields.put(it.id, it.name);
      }

      return availableFields;
    }    

    /**
     * Indicates whether the provided String is numeric.
     *
     * @param value
     *        The String to be evaluated.
     *
     * @return
     *        true if the String is numeric, otherwise false.
     */
    private boolean isNumeric(String value){
      try {
           NumberFormat formatter = NumberFormat.getInstance();
           ParsePosition pos = new ParsePosition(0);
           formatter.parse(value, pos);

           return value.length() == pos.getIndex();
        } catch (ParseException pEx) {
           return false;
        }
    }

    /** 
     * Verifies the "Default Value" parameter.
     *
     * @param parameterValue
     *          The value to be validated.
     * @param errorKey
     *          The key used to group error messages for a specific parameter.
     * @return
     *          Where a one or more validation errors are found, an 
     *          ErrorCollection instance will be returned containing
     *          the list of errors.
     *          
     *          If no errors are found, returns null.
     *
     * @throw IllegalArgumentException
     *          Thrown if parameterValue or errorKey are null or empty.
     */
    private ErrorCollection validateNumericParameter(String parameterValue, String errorKey) {
        ErrorCollection errors = new SimpleErrorCollection();

        if (!parameterValue || parameterValue.trim().isEmpty()) {
            String exceptionMessage = String.format(ExceptionMessages.ARGUMENT_NULL, "parameterValue");
            throw new IllegalArgumentException(exceptionMessage);
        }

        if (!errorKey || errorKey.trim().isEmpty()){
            String exceptionMessage = String.format(ExceptionMessages.ARGUMENT_NULL, "errorKey");
            throw new IllegalArgumentException(exceptionMessage);
        }

        if (!this.isNumeric(parameterValue)){
            errors.addError(errorKey, UIMessages.VALUE_NOT_NUMERIC);
        }

        // String contains a period, so assume it's a Double and try to parse
        if (parameterValue.contains(".")) {
            try {
                Double parameterValueAsDouble = Double.parseDouble(parameterValue);
            } catch (NumberFormatException nfEx) {
                errors.addError(errorKey, UIMessages.VALUE_NOT_NUMERIC);
            }
        } else {
            // String doesn't contain a period, so assume it's an Integer and try to parse
            try {
                Integer parameterValueAsInt = Integer.parseInt(parameterValue);
            } catch (NumberFormatException nfEx) {
                errors.addError(errorKey, UIMessages.VALUE_NOT_NUMERIC);
            }
        }

        return errors.hasAnyErrors() ? errors : null;
    }



    /**
     * Helper class that contains the exception messages raised by the
     * DecrementCustomField class.
     */
    private static class ExceptionMessages {
      public static final String ARGUMENT_NULL = "%s must be provided.";
    }



    /**
     * Helper class that contains the messages written to the JIRA logs
     * by the DecrementCustomField class.
     */
    private static class LogMessages {
        public static final String CATEGORIES = "The categories for the %s post function are - %s";    
        public static final String ISSUE_NOT_FOUND = "Failed to retrieve the issue, unable to continue.";
        public static final String POSTFUNCTION_CONDITION_FAILED = "Not decrementing %s because the post-function condition failed.";
        public static final String VALIDATING_PARAMETERS = "Validating the parameters. Parameters are - %s";    
    }



    /**
     * Helper class that contains the messages that can be displayed in the UI
     * by the DecrementCustomField class.
     */
    private static class UIMessages {
        public static final String DESCRIPTION = "Decrements the value of a custom field.";
        public static final String HELP_URL = "http://nathonfowlie.id.au";
        public  static final String NAME = "Decrement Custom Field";
        public static final String DETAILED_DESCRIPTION = "<strong>%s</strong> will be decremented by <strong>%s</strong>. If it does not contain a value, it will be set to <strong>%s</strong> then decremented by <strong>%s</strong>";
        public static final String CUSTOMFIELD_IS_REQUIRED = "You must specify the custom field to be decremented.";
        public static final String DECREMENT_IS_REQUIRED =  "You must specify the value to decrement the custom field by.";
        public static final String DECREMENT_NOT_NUMERIC = "The decrement value must be numeric.";
        public static final String VALUE_NOT_NUMERIC = "The value must be numeric.";
    }
}