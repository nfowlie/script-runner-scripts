package au.id.nathonfowlie.jira.utils;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.ModifiedValue;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.util.IssueChangeHolder;
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder;

import id.au.nathonfowlie.jira.scriptrunner.postfunctions.ParameterDescriptor;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.ParsePosition;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 * Utility class that can be used to perform math operations against an issues
 * CustomField.
 *
 * @author Nathon Fowlie &lt;nathon.fowlie&#64;gmail.com&gt;
 * @since 21/03/2014
 * @version 1.0
 */
public class CustomFieldMathOperation {
    private static final Logger log = Logger.getLogger(CustomFieldMathOperation.class);

    private final CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
    private final IssueChangeHolder issueChangeHolder = new DefaultIssueChangeHolder();

    private MutableIssue issue = null;

    public static final String ARGUMENT_NULL = "%s must be provided.";

    /**
     * Initializes a new instance of the CustomFieldMathOperation class.
     *
     * @param issue
     *          The issue that the math operation will be performed against.
     *
     * @throw IllegalArgumentException
     *          Thrown if issue is null.
     */
    public CustomFieldMathOperation(MutableIssue issue) {
        if (!issue) {
            String exceptionMessage = String.format(ARGUMENT_NULL, "issue");
            throw new IllegalArgumentException(exceptionMessage);
        }

        this.issue = issue;
    }

    /**
     * Decrements the value of a custom field by the specified amount.
     *
     * Where the custom field does not have a value, it will be set to
     * provided seedValue and then decremented. If seedValue is null
     * and the custom field does not have a value, then the custom field
     * will be left as is (ie: set to null).
     *
     * @param customField
     *        The CustomField to be updated.
     * @param decrement
     *        An Integer that specifies the amount to decrement the 
     *        CustomField value by.
     * @param seedValue
     *        An Integer that specified the seed value to be used if
     *        the CustomField does not have a value.
     *
     * @throw IllegalArgumentException
     *        Thrown if customField or decrementAmount are null or empty.
     */
    public  void decrementCustomField(CustomField customField, Integer decrementAmount, Integer seedValue) {
        String newValue = (seedValue - decrementAmount).toString();

        if (!customField) {
            String exceptionMessage = String.format(ARGUMENT_NULL, "customField");
            throw new IllegalArgumentException(exceptionMessage);
        }

        if (!decrementAmount) {
            String exceptionMessage = String.format(ARGUMENT_NULL, "decrementAmount");
            throw new IllegalArgumentException(exceptionMessage);
        }

        String currentValue = (String) this.getIssue().getCustomFieldValue(customField);

        if (!currentValue || currentValue.trim().isEmpty()) {
                currentValue = seedValue.toString();
        }

        try {
            Integer intToDecrement = Integer.parseInt(currentValue);
            intToDecrement -= decrementAmount;
            newValue = intToDecrement.toString();
        } catch (NumberFormatException nfEx) {
            log.warn(nfEx.getMessage());
        }

        this.updateCustomField(customField, newValue);
    }

    /**
     * Decrements the value of a custom field by the specified amount.
     *
     * Where the custom field does not have a value, it will be set to
     * provided seedValue and then decremented. If seedValue is null
     * and the custom field does not have a value, then the custom field
     * will be left as is (ie: set to null).
     *
     * @param customField
     *        The CustomField to be updated.
     * @param decrement
     *        A Double that specifies the amount to decrement the 
     *        CustomField value by.
     * @param seedValue
     *        A Double that specified the seed value to be used if
     *        the CustomField does not have a value.
     *
     * @throw IllegalArgumentException
     *        Thrown if customField or decrementAmount are null or empty.
     */
    public void decrementCustomField(CustomField customField, Double decrementAmount, Double seedValue) {
        String newValue = (seedValue - decrementAmount).toString();

        if (!customField) {
            String exceptionMessage = String.format(ARGUMENT_NULL, "customField");
            throw new IllegalArgumentException(exceptionMessage);
        }

        if (!decrementAmount) {
            String exceptionMessage = String.format(ARGUMENT_NULL, "decrementAmount");
            throw new IllegalArgumentException(exceptionMessage);
        }

        String currentValue = (String) this.getIssue().getCustomFieldValue(customField);

        if (!currentValue || currentValue.trim().isEmpty()) {
                currentValue = seedValue.toString();
        }

        try {
            Double doubleToDecrement = Double.parseDouble(currentValue);
            doubleToDecrement -= decrementAmount;
            newValue = doubleToDecrement.toString();
        } catch (NumberFormatException nfEx) {
            log.warn(nfEx.getMessage());
        }

        this.updateCustomField(customField, newValue);
    }

    /**
     * Decrements the value of a custom field by the specified amount.
     *
     * Where the custom field does not have a value, it will be set to
     * provided seedValue and then decremented. If seedValue is null
     * and the custom field does not have a value, then the custom field
     * will be left as is (ie: set to null).
     *
     * @param customField
     *        The CustomField to be updated.
     * @param decrement
     *        A String  that specifies the amount to decrement the 
     *        CustomField value by.
     * @param seedValue
     *        A String that specified the seed value to be used if
     *        the CustomField does not have a value.
     *
     * @throw IllegalArgumentException
     *        Thrown if customField or decrementAmount are null or empty.
     */
    public void decrementCustomField(CustomField customField, String decrementAmount, String seedValue) {
        if (!customField) {
            throw new IllegalArgumentException(String.format(ARGUMENT_NULL, "customField"));
        }

        if (!decrementAmount || decrementAmount.trim().isEmpty()) {
            throw new IllegalArgumentException(String.format(ARGUMENT_NULL, "decrementAmount"));
        }

        if (!this.validateNumericParameter(decrementAmount)) {
            return;
        }

        if (seedValue) {
            if (!this.validateNumericParameter(seedValue)) {
                return;
            }
        } else {
            seedValue = ""; // Make sure seed value is never null...
        }

        // Seed value was defined, so do a bit of extra type conversion then 
        // pass off to the relevant overloaded method.
        if (seedValue.contains(".")) {
            try {
                Double seedValueAsDouble = Double.parseDouble(seedValue);
                Double decrementAsDouble = Double.parseDouble(decrementAmount);

                this.decrementCustomField(customField, decrementAsDouble, seedValueAsDouble);
            } catch (NumberFormatException nfEx) {
                log.warn(nfEx.getMessage());
            }
        } else {
            try {
                Integer seedValueAsInt = Integer.parseInt(seedValue);
                Integer decrementAsInt = Integer.parseInt(decrementAmount);

                this.decrementCustomField(customField, decrementAsInt, seedValueAsInt);
            } catch (NumberFormatException nfEx) {
                log.warn(nfEx.getMessage());
            }
        }
    }

    /** 
     * Gets the issue that the math operation should be performed against.
     *
     * @return
     *           A MutableIssue.
     */
    private MutableIssue getIssue() {
            return this.issue;
    }

    /**
     * Gets the IssueChangeHolder used to notify JIRA that an issue has been 
     * modified.
     *
     * @return
     *          An IssueChangeHolder instance.
     */
    private IssueChangeHolder getIssueChangeHolder() {
            return this.issueChangeHolder;
    }

    /**
     * Increments the value of a custom field by the specified amount.
     *
     * Where the custom field does not have a value, it will be set to
     * provided seedValue and then incremented. If seedValue is null
     * and the custom field does not have a value, then the custom field
     * will be left as is (ie: set to null).
     *
     * @param customField
     *        The CustomField to be updated.
     * @param decrement
     *        An Integer that specifies the amount to increment the 
     *        CustomField value by.
     * @param seedValue
     *        An Integer that specified the seed value to be used if
     *        the CustomField does not have a value.
     *
     * @throw IllegalArgumentException
     *        Thrown if customField or incrementAmount are null or empty.
     */
    public void incrementCustomField(CustomField customField, Integer incrementAmount, Integer seedValue) {
        String newValue = (seedValue + incrementAmount).toString();

        if (!customField) {
            String exceptionMessage = String.format(ARGUMENT_NULL, "customField");
            throw new IllegalArgumentException(exceptionMessage);
        }

        if (!incrementAmount) {
            String exceptionMessage = String.format(ARGUMENT_NULL, "incrementAmount");
            throw new IllegalArgumentException(exceptionMessage);
        }

        String currentValue = (String) this.getIssue().getCustomFieldValue(customField);

        if (!currentValue || currentValue.trim().isEmpty()) {
                currentValue = seedValue.toString();
        }

        try {
            Integer intToIncrement = Integer.parseInt(currentValue);
            intToIncrement += incrementAmount;
            newValue = intToIncrement.toString();
        } catch (NumberFormatException nfEx) {
            log.warn(nfEx.getMessage());
        }

        this.updateCustomField(customField, newValue);
    }

    /**
     * Increments the value of a custom field by the specified amount.
     *
     * Where the custom field does not have a value, it will be set to
     * provided seedValue and then incremented. If seedValue is null
     * and the custom field does not have a value, then the custom field
     * will be left as is (ie: set to null).
     *
     * @param customField
     *        The CustomField to be updated.
     * @param decrement
     *        A Double that specifies the amount to increment the 
     *        CustomField value by.
     * @param seedValue
     *        A Double that specifies the seed value to be used if
     *        the CustomField does not have a value.
     *
     * @throw IllegalArgumentException
     *        Thrown if customField or incrementAmount are null or empty.
     */
    public void incrementCustomField(CustomField customField, Double incrementAmount, Double seedValue) {
        String newValue = (seedValue + decrementAmount).toString();

        if (!customField) {
            String exceptionMessage = String.format(ARGUMENT_NULL, "customField");
            throw new IllegalArgumentException(exceptionMessage);
        }

        if (!incrementAmount) {
            String exceptionMessage = String.format(ARGUMENT_NULL, "incrementAmount");
            throw new IllegalArgumentException(exceptionMessage);
        }

        String currentValue = (String) this.getIssue().getCustomFieldValue(customField);

        if (!currentValue || currentValue.trim().isEmpty()) {
                currentValue = seedValue.toString();
        }

        try {
            Double doubleToIncrement = Double.parseDouble(currentValue);
            doubleToIncrement += incrementAmount;
            newValue = doubleToIncrement.toString();
        } catch (NumberFormatException nfEx) {
            log.warn(nfEx.getMessage());
        }

        this.updateCustomField(customField, newValue);
    }

    /**
     * Increments the value of a custom field by the specified amount.
     *
     * Where the custom field does not have a value, it will be set to
     * provided seedValue and then incremented. If seedValue is null
     * and the custom field does not have a value, then the custom field
     * will be left as is (ie: set to null).
     *
     * @param customField
     *        The CustomField to be updated.
     * @param decrement
     *        A String  that specifies the amount to increment the 
     *        CustomField value by.
     * @param seedValue
     *        A String that specifies the seed value to be used if
     *        the CustomField does not have a value.
     *
     * @throw IllegalArgumentException
     *        Thrown if customField or incrementAmount are null or empty.
     */
    public void incrementCustomField(CustomField customField, String incrementAmount, String seedValue) {
        if (!customField) {
            throw new IllegalArgumentException(String.format(ARGUMENT_NULL, "customField"));
        }

        if (!incrementAmount || incrementAmount.trim().isEmpty()) {
            throw new IllegalArgumentException(String.format(ARGUMENT_NULL, "incrementAmount"));
        }

        if (!this.validateNumericParameter(incrementAmount)) {
            return;
        }        

        if (seedValue) {
            if (!this.validateNumericParameter(seedValue)) {
                return;
            }
        } else {
            seedValue = ""; // Seed value should never be null.
        }

        // Seed value was defined, so do a bit of extra type conversion then 
        // pass off to the relevant overloaded method.
        if (seedValue.contains(".")) {
            try {
                Double seedValueAsDouble = Double.parseDouble(seedValue);
                Double incrementAsDouble = Double.parseDouble(incrementAmount);

                this.incrementCustomField(customField, incrementAsDouble, seedValueAsDouble);
            } catch (NumberFormatException nfEx) {
                log.warn(nfEx.getMessage());
            }
        } else {
            try {
                Integer seedValueAsInt = Integer.parseInt(seedValue);
                Integer incrementAsInt = Integer.parseInt(incrementAmount);

                this.incrementCustomField(customField, incrementAsInt, seedValueAsInt);
            } catch (NumberFormatException nfEx) {
                log.warn(nfEx.getMessage());
            }
        }
    }

    /**
     * Indicates whether the provided String is numeric.
     *
     * @param value
     *        The String to be evaluated.
     *
     * @return
     *        true if the String is numeric, otherwise false.
     */
    private boolean isNumeric(String value){
      try {
           NumberFormat formatter = NumberFormat.getInstance();
           ParsePosition pos = new ParsePosition(0);
           formatter.parse(value, pos);

           return value.length() == pos.getIndex();
        } catch (ParseException pEx) {
           return false;
        }
    }

    /**
     * Updates the value of a custom field.
     *
     * @param customField
     *          The CustomField to be updated.
     * @param newValue
     *          The new value to assign to the CustomField.
     *
     * @throw IllegalArgumentException
     *          Thrown if customField is null.
     */
    private void updateCustomField(CustomField customField, String newValue) {
        if (!customField) {
            String exceptionMessage = String.format(ARGUMENT_NULL, "customField");
            throw new IllegalArgumentException(exceptionMessage);
        }

        IssueChangeHolder issueChangeHolder = new DefaultIssueChangeHolder();

        ModifiedValue modifiedValue = new ModifiedValue(
            this.getIssue().getCustomFieldValue(customField), 
            newValue
        );

        customField.updateValue(null, this.getIssue(), modifiedValue,this.getIssueChangeHolder());
    }

    /** 
     * Verifies the "Default Value" parameter.
     *
     * @param parameterValue
     *          The value to be validated.
     *
     * @return
     *          true on success, otherwise false.
     *
     * @throw IllegalArgumentException
     *          Thrown if parameterValue is null or empty.
     */
    private boolean validateNumericParameter(String parameterValue) {
        if (!parameterValue || parameterValue.trim().isEmpty()) {
            String exceptionMessage = String.format(ARGUMENT_NULL, "parameterValue");
            throw new IllegalArgumentException(exceptionMessage);
        }

        if (!this.isNumeric(parameterValue)){
            return false;
        }

        // String contains a period, so assume it's a Double and try to parse
        if (parameterValue.contains(".")) {
            try {
                Double parameterValueAsDouble = Double.parseDouble(parameterValue);
            } catch (NumberFormatException nfEx) {
                return false;
            }
        } else {
            // String doesn't contain a period, so assume it's an Integer and try to parse
            try {
                Integer parameterValueAsInt = Integer.parseInt(parameterValue);
            } catch (NumberFormatException nfEx) {
                return false;
            }
        }

        return true;
    }
}