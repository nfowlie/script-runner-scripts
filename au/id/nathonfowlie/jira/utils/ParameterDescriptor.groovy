package au.id.nathonfowlie.jira.scriptrunner.postfunctions;

/**
 * This class defines the properties of a post-function parameter used by the
 * Script-Runner plugin.
 *
 * @author Nathon Fowlie &lt;nathon.fowlie&#64;gmail.com&gt;
 * @since 21/03/2014
 * @version 1.0
 */
public  class ParameterDescriptor {
    private String name = null;
    private String label = null;
    private String fieldType = null;
    private String description = null;

    public static final String ARGUMENT_NULL = "%s must be provided.";

    /**
     * Initializes a new instance of the ParameterDescriptor class.
     *
     * @param name
     *          The name of the post-function.
     * @param label
     *          The post-function label as it will appear on the post-functions
     *          configuration screen.
     * @param fieldType
     *          The type of input control that will be used to allow the user to
     *          set the parameter value. Refer to the Script runner documentation
     *          for a list of available options.
     * @param description
     *          A brief description of the post-function.
     *
     * @throw IllegalArgumentException
     *          Thrown if the name, label, fieldType or description parameters
     *          are null or empty.
     */
    public ParameterDescriptor(String name, String label, String fieldType, String description) {
        if (!name || name.trim().isEmpty()) {
          throw new IllegalArgumentException(String.format(ARGUMENT_NULL, "name"));
        }

        if (!label || label.trim().isEmpty()) {
          throw new IllegalArgumentException(String.format(ARGUMENT_NULL, "label"));
        }

        if (!fieldType || fieldType.trim().isEmpty()) {
          throw new IllegalArgumentException(String.format(ARGUMENT_NULL, "fieldType"));
        }

        if (!description || description.trim().isEmpty()) {
          throw new IllegalArgumentException(String.format(ARGUMENT_NULL, "description"));
        }

        this.name = name;
        this.label = label;
        this.fieldType = fieldType;
        this.description = description;
    }

    /**
     * Gets the name of the post-function parameter. This will be used as the
     * key in the Map<String, Object> that will be passed to the doScript(Map)
     * method of the post-function script.
     */
    public String getName(){
        return this.name;
    }

    /**
     * Gets the post-function parameter label as it will appear on the
     * post-function configuration screen.
     *
     * @return
     *      The post-function label.
     */
    public String getLabel() {
        return this.label;
    }

    /**
     * Gets the type of input control that should be used to allow the user to
     * set the post-function parameter value.
     *
     * @return
     *      A String that describes the type of input control. Refer to the
     *      Script Runner documentation for a list of possible values.
     */
    public String getFieldType() {
        return this.fieldType;
    }

    /**
     * Gets a brief description of the post-function parameter.
     *
     * @return
     *      The post-function parameter description.
     */
    public String getDescription() {
        return this.description;
    }    
}