/**
 * This script populates a custom field of type "Select List - Single Choice" with
 * all available Versions defined for a particular project, where the project is
 * specified in a custom field of type "Project Picker (single project)".
 *
 * The script has been tested with JIRA 6.1.5, using the Behaviours v0.5.10 plugin.
 *
 * Installation/Configuration Instructions:
 * ----------------------------------------------------------------------------
 *  1) Install the Behaviours plugin
 *     (https://marketplace.atlassian.com/plugins/com.onresolve.jira.plugin.Behaviours).
 *  2) Create a custom field of type "Project Picker (single project)". This will
 *     be used to drive the "Build Versions" dropdownlist.
 *  3) Create a custom field of type "Select List - Single Choice". The field
 *     should be configured as mandatory, with a default option of "None".
 *  4) Copy this script to
 *     [install directory]/atlassian-jira/WEB-INF/classes/au/id/nathonfowlie/jira/behaviours.
 *  5) Change the unique identifiers for the PROJECT_LINK_FIELD_ID and BUILD_VERSION_FIELD_ID
 *     constants to the correct values. You can find the unique identifier of the custom fields
 *      by hovering your mouse over the "edit" link on the "Custom Fields" configuration page
 *      in JIRA (http://jira_url/secure/admin/ViewCustomFields.jspa).
 *  4) Create a new Behaviour and add a mapping to specify which projects and/or
 *     issue types the behaviour will be associated with.
 *  5) Add the "Project Link" field to the behaviour with the following
 *     configuration
 *        - Do not enable the validator plugin.
 *        - Field is required.
 *        - Field is writable.
 *        - Field is visible.
 *        - Add a server-side script and set the class name to
 *        - Do not add a condition.
 *        - Add a serverside script and set "Class or file" to the full path of
 *          this script, and "Method" to "PopulateDropDown"/.
 *  6) Create a test issue. When you change the selected project, the build revision
 *     dropdownlist should automatically display the list of available versions for
 *     that project.
 *
 * @author Nathon Fowlie &lt;nathon.fowlie@gmail.com&gt;
 * @version 1.0
 * @since 21/01/2014
 */

package id.au.nathonfowlie.jira.behaviours;

import com.atlassian.jira.component.ComponentAccessor;
import com.onresolve.jira.groovy.user.FieldBehaviours;
import com.onresolve.jira.groovy.user.FormField;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.customfields.manager.OptionsManager;
import com.atlassian.jira.issue.customfields.option.*;
import com.atlassian.jira.issue.fields.*;
import com.atlassian.jira.issue.fields.config.*;
import com.atlassian.jira.project.*;
import com.atlassian.jira.project.version.*;
import org.apache.log4j.*;

public class BuildVersionSelector extends FieldBehaviours {
    // This should be the unique id of the Project Selector custom field that allows
    // the user to specify which project the Release issue type represents (custom
    // field is probably called "Project Link").
    private static final long PROJECT_LINK_FIELD_ID  = 10403;

    // This should be the unique id of the single select custom field that
	// allows the user to specify which build the Release issue type
    // represents (ie: the "Build Revision" field).
    private static final long BUILD_VERSION_FIELD_ID = 10900;

	// Various JIRA manager Objects for CRUD operations
    private CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager();
    private OptionsManager optionsManager = ComponentAccessor.getOptionsManager();
    private ProjectManager projectManager = ComponentAccessor.getProjectManager();
    private VersionManager versionManager = ComponentAccessor.getVersionManager();

    // This is the default list of field options that should be available where
    // the selected project does not have any defined versions.
    private static final defaultFieldOptions;



    /**
     * Constructor. This will ensure that the default drop down list options
	 * are available.
     */
    public BuildVersionSelector() {
        defaultFieldOptions = new HashMap<String, String>();
        defaultFieldOptions.put("0", "None");

        this.ensureDefaultFieldOptions();
    }



    /**
     * Ensures that DropDownList options exist for each Version in the selected
	 * project.
     *
     * If the selectedProject or existingOptions parameter is null, any project
     * versions not already in the build version DropDownList will NOT be added
     * and a warning will be written to the application logs.
     *
     * @param selectedProject   A Project Object that represents the project
     *                          that the release version represents.
     * @param existingOptions   The list of existing build version DropDownList options. This
     *                          is used to determine which project versions are missing from
     *                          the DropDownList.
     */
    private void addMissingProjectVersions(Project selectedProject, Options existingOptions){
        if (selectedProject == null) {
            this.log.warn("Cannot add missing project versions to the build version DropDownList because the selected project was not specified.");
            return;
        }

        if (existingOptions == null) {
            this.log.warn("Cannot add missing project versions to the build version DropDownList because the list of existing DropDownList options was not specified.");
            return;
        }

        Collection<Version> versions = this.versionManager.getVersions(selectedProject.getId());

        Option versionOption = null;

        if (versions == null || versions.size() == 0) {
            return;
        }

        for (version in versions) {
            versionOption = existingOptions.getOptionForValue(version.getName(),null);

            if (versionOption == null) {
                this.log.debug(String.format("Adding new dropdown list option with value '%s'", version.getName()));

                if (existingOptions.addOption(null, version.getName()) == null) {
                    this.log.warn(String.format("Failed to add new build version drop down option for project version %s", version.getName()));
                }
            }
        }
    }



    /**
     * Ensures that the build version field has dropdown option values for the
     * list of default options.
     */
    private void ensureDefaultFieldOptions() {
        FieldConfig fieldConfig = this.getCustomFieldConfiguration(BUILD_VERSION_FIELD_ID);

        if (fieldConfig == null) {
            this.log.error(String.format("Failed to set default field options, could not find a field configuration for field with id %d", BUILD_VERSION_FIELD_ID));
            return;
        }

        Options buildVersionOptions = this.optionsManager.getOptions(fieldConfig);

        // Make sure there are Option records for each default dropdown option.
        for (defaultFieldOption in defaultFieldOptions) {
            if (buildVersionOptions.getOptionForValue("None", null) == null) {
                buildVersionOptions.addOption(null, "None");
            }
        }
    }



	/**
     * Generates the list of drop down options displayed in the build version
     * DropDownList.
     *
     * @param currentProject  A Project Object that represents the project that
     *                        the issue represents.
     * @param existingOptions An Options object that contains the existing list
     *                        of build version drop down options.
     *
     * @return A HashMap&lt;String, String&gt; that contains the array of
     *          drop down options to be displayed in the build version
     *          DropDownList.
     */
     private HashMap<String, String> filterDropDownListForProject(Project currentProject, Options existingOptions) {
        Map<String, String> fieldOptions = new HashMap<String, String>();

        Collection<Version> versions = this.versionManager.getVersions(currentProject.getId());

        Option versionOption = null;

        if (versions != null && versions.size() > 0) {
            for (version in versions) {
                versionOption = existingOptions.getOptionForValue(version.getName(),null);

                if (versionOption != null) {
                    fieldOptions.put(versionOption.getOptionId().toString(), versionOption.getValue());
                }
            }
        } else {
            this.log.debug(String.format("No versions are defined for the %s project. Falling back to default values", currentProject.getName()));

            fieldOptions = defaultFieldOptions;
        }

        return fieldOptions;
    }



	/**
     * Gets the applicable field configuration for a specific field.
     *
     * @param fieldId The unique id of the custom field.
     *
     * @return A FieldConfig object applicatble for the current issue type and
     * project, or null if a FieldConfig cannot be found.
     */
    private FieldConfig getCustomFieldConfiguration(long fieldId) {
        if (fieldId <= 0) {
            return null;
        }

        CustomField customField = this.customFieldManager.getCustomFieldObject(fieldId);

        if (customField == null) {
            this.log.warn(String.format("Failed to locate custom field with id %s.", fieldId));
            return null;
        }

        List<FieldConfigScheme> configurationSchemes = customField.getConfigurationSchemes();

        if (configurationSchemes == null) {
            this.log.warn(String.format("Failed to find configuration schemes for %s.", customField.getFieldName()));
            return null;
        }

        return configurationSchemes.listIterator().next().getOneAndOnlyConfig();
    }



	/**
     * Gets the project for the current issue.
     *
     * @param projectId The unique id of the selected project.
     *
     * @return A Project object that the current issue will be created in, or
     *          null if the project cannot be determined.
     */
     private Project getProject(String projectId) {
        if (projectId == null || projectId.isEmpty()) {
            this.log.warn("Project id was null or empty, unable to determine current project");
            return null;
        }

        try {
            long id = Long.parseLong(projectId);

            this.log.debug(String.format("Setting project id to %d", id));

            return this.projectManager.getProjectObj(id);
        } catch(NumberFormatException nfEx) {
            this.log.warn(nfEx.getMessage());
            return;
        }
    }



	 /**
     * Populates the build version drop down list with a list of Versions
     * (released or unreleased) that are defined in the project selected
     * in the Project Link drop down list.
     */
     public void populateBuildVersions() {
        this.log.debug(String.format("Attempting to populate the build version dropdown list. Project Link field id is '%s' and Build Version field id is '%s'.", PROJECT_LINK_FIELD_ID, BUILD_VERSION_FIELD_ID));

        // Grab the value of the build version form field and load the field configuration
        CustomField buildVersionField = this.customFieldManager.getCustomFieldObject(BUILD_VERSION_FIELD_ID);

        if (buildVersionField == null) {
            this.log.warn(String.format("Failed to locate the build version field with id %s.", BUILD_VERSION_FIELD_ID));
            return;
        }

        FieldConfig fieldConfig = this.getCustomFieldConfiguration(BUILD_VERSION_FIELD_ID);

        if (fieldConfig == null) {
            return;
        }

        Options buildVersionOptions = this.optionsManager.getOptions(fieldConfig);

        if (buildVersionOptions == null) {
            this.log.warn(String.format("Failed to retrieve the list of options available to the %s custom field for project %s.", buildVersionField.getFieldName(), project.getName()));
            return;
        }

        FormField buildVersionFormField = getFieldById(String.format("customfield_%s", BUILD_VERSION_FIELD_ID));

        if (buildVersionFormField == null) {
            this.log.error(String.format("Unable to find form fields for customfield_%s.", BUILD_VERSION_FIELD_ID));
            return;
        }

        // Grab the UI controls and find the unique id for the selected project
        FormField projectLinkFormField = getFieldById(String.format("customfield_%s", PROJECT_LINK_FIELD_ID));

        if (projectLinkFormField == null) {
            this.log.warn(String.format("Unable to find form fields for customfield_%s.", PROJECT_LINK_FIELD_ID));
           buildVersionFormField.setFieldOptions(defaultFieldOptions);
            return;
        }

        Project project = this.getProject(projectLinkFormField.getValue());

        if (project == null) {
            buildVersionFormField.setFieldOptions(defaultFieldOptions);
            return;
        }

        this.addMissingProjectVersions(project, buildVersionOptions);

        // Refresh the build version options list, just in case the previous
        // method call added some new options
        buildVersionOptions = this.optionsManager.getOptions(fieldConfig);

        HashMap<String, String> fieldOptions = this.filterDropDownListForProject(project, buildVersionOptions);
        buildVersionFormField.setFieldOptions(fieldOptions);
    }
}
