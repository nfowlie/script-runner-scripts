/*
 * Description:  This script will set the rank of existing issues based on a JSON source file that 
 *               contains an ordered list of issues from another JIRA instance.
 * Author:       Nathon Fowlie <nathon.fowlie@gmail.com>
 * Version:      0.2
 * Created:      14/01/2013 Nathon Fowlie
 * Last Updated: 14/01/2013 Nathon Fowlie
 *
 * To Do:        Add functionality to pull order list of issues from another JIRA instance instead of
 *               relying on a JSON file.
 */

import com.atlassian.jira.component.*;
import com.atlassian.plugin.*;
import com.atlassian.jira.issue.*;
import com.atlassian.jira.security.*;
import com.atlassian.jira.user.*;
import com.atlassian.crowd.embedded.api.*;
import groovy.json.JsonSlurper;

// The field id of the "Rank" custom field. There could be multiple "Rank" fields, so you need
// to explicitly specify which one the script should use.
final int RANK_FIELD_ID = 10104;

// The relative or absolute path to the JSON file that contains the ordered list of issues.
final String dataFile = "/my/data/file.json";

JsonSlurper slurper = new JsonSlurper();

BufferedReader reader = new BufferedReader(new FileReader(dataFile));
def data = slurper.parse(reader);

PluginAccessor pluginAccessor = ComponentAccessor.getPluginAccessor();
IssueManager issueManager = ComponentAccessor.getIssueManager();
JiraAuthenticationContext authContext = ComponentAccessor.getJiraAuthenticationContext();

Class greenHopperClass = pluginAccessor.getClassLoader().findClass("com.pyxis.greenhopper.GreenHopper");
def greenHopper = componentManager.getOSGiComponentInstanceOfType(greenHopperClass);
 
if (!greenHopper ) {
  return "GreeenHopper not found";
}

Class rankManagerClass = pluginAccessor.getClassLoader().findClass("com.atlassian.greenhopper.api.rank.RankService");

def rankManager = componentManager.getOSGiComponentInstanceOfType(rankManagerClass);
    
if (!rankManager) {
    return "RankManager not found";
}

for (def record : data.issues) {
    Issue currentIssue = issueManager.getIssueObject(record.key);

    if (!currentIssue) {
        continue;
    }    
    
    com.atlassian.crowd.embedded.api.User currentUser = authContext.getUser().getDirectoryUser();
    
    def outcome = rankManager.rankFirst(currentUser, RANK_FIELD_ID, currentIssue);

    if (!outcome.valid) {
        return "Invalid outcome for " + currentIssue.getId();;
    }
}